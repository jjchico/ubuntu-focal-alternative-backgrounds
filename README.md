# Modified versions of Ubuntu 20.04 (Focal Fossa) default wallpaper

Motivation
----------

I did not like the ray-like dots. Also, I prefer darker wallpapers.

Original art
------------

As found in an Ubuntu 20.04 installation:

* File: /usr/share/backgrounds/warty-final-ubuntu.png
* Package: ubuntu-wallpapers (version 20.04.2-0ubuntu1)
* Copyright: 2016-2020 Canonical Ltd
* License: CC-BY-SA-4.0 (see /usr/share/doc/ubuntu-wallpapers/copyright)

Author and licence
------------------

* Jorge Juan-Chico <jjchico@gmail.com>
* Licence: CC-BY-SA-4.0



